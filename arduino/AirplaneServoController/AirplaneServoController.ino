#include<Servo.h>

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

Servo servo[13];

void setup() {
  
  for(int i = 0; i < 13; i++) {
    servo[i].attach(i);
    servo[i].write(0);
  }
  
  Serial.begin(9600);
  Serial.println();
}

String incoString = "";
unsigned long StartTime = 0;


void loop() {
  
  // send data only when you receive data:
  if (Serial.available() > 0) {

    if(StartTime == 0)
      StartTime = millis();
    // read the incoming:
    char incoming= char(Serial.read());
    // say what you got:
    incoString += incoming;

    
    if(incoming == '\n'){
      
      int servoNum, angle;
      servoNum = getValue(incoString,',', 0).toInt();
      angle = getValue(incoString,',', 1).toInt();

      servo[servoNum].write(angle);
      
      incoString = "";
      Serial.println(millis() - StartTime);
      StartTime = 0;
    }
  } 
}
