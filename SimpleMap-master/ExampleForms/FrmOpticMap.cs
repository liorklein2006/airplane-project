﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Text;
using System.Net.NetworkInformation;

using System.Net;      //required
using System.Net.Sockets;
using ProgramMain.ExampleForms.Controls;
using System.Threading;
using System.Diagnostics;
using AvionicsInstrumentControlDemo;
using CefSharp;
using CefSharp.WinForms;
using System.Collections.Generic;

namespace ProgramMain.ExampleForms
{

    public partial class FrmOpticMap : Form
    {
        public static bool isNeedToRun = true;
        public static int PORT = 5555;
        public static FrmOpticMap form;
        public Thread someThread;
        public static DemoWinow ivonics;

        public FrmOpticMap()
        {

            InitializeComponent();

            //BrowserView browserView = new WinFormsBrowserView();
            //panel1.Controls.Add((Control)browserView);
            //browserView.Browser.LoadURL("http://www.bbc.com");

            //geckoWebBrowser1.Navigate("www.bbc.com");


            Icon = Miscellaneous.DefaultIcon();
            form = this;
            //mapCtl1.MoveCenterMapObject((decimal)35.016816, (decimal)32.742071);
            mapCtl1.Level = 17;

            //graph graphForm = new graph();

            //// Show the graphy form
            //graphForm.Show();

            ivonics = new DemoWinow();
            ivonics.Show();
            Cef.Initialize(new CefSettings());
            InitBrowser("www.google.com");

            comboBox1.DataSource = net_adapters();
        }

        public ChromiumWebBrowser browser;
        public void InitBrowser(String url)
        {
            browser = new ChromiumWebBrowser(url);
            panel1.Controls.Clear();
            panel1.Controls.Add((Control)browser);
            browser.Dock = DockStyle.Fill;
        }

        public System.Collections.Generic.List<String> net_adapters()
        {
            List<String> values = new List<String>();
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            values.Add(ip.Address.ToString());
                        }
                    }
                }
            }
            return values;
        }

        public static IPAddress StrToIp(string ip)
        {
            return new IPAddress(new byte[] {
                            Convert.ToByte(ip.Substring(0,2), 16), Convert.ToByte(ip.Substring(2,2), 16),
                            Convert.ToByte(ip.Substring(4,2), 16), Convert.ToByte(ip.Substring(6,2), 16)}
                                        );
        }

        public void StartServer()
        {
            try
            {
                if (!isNeedToRun)
                {
                    MessageBox.Show("is need to run is False .. please fix this , the server may not run");
                }
                IPAddress ip = System.Net.IPAddress.Parse((string)comboBox1.SelectedValue);
                TcpListener server = new TcpListener(IPAddress.Any, PORT);

                server.Start();  // this will start the server

                new Thread(() =>
                {

                    while (isNeedToRun)   //we wait for a connection
                    {
                        try
                        {
                            TcpClient client = server.AcceptTcpClient();  //if a connection exists, the server will accept it

                            form.connStatus.Invoke((MethodInvoker)delegate
                            {
                                // Running on the UI thread
                                changeToConnected();
                            });

                            pingThread(client); 

                            NetworkStream ns = client.GetStream(); //networkstream is used to send/receive messages

                            //byte[] hello = new byte[100];   //any message must be serialized (converted to byte array)
                            //hello = Encoding.Default.GetBytes("hello world");  //conversion string => byte array

                            //ns.Write(hello, 0, hello.Length);     //sending the message

                            while (client.Connected)  //while the client is connected, we look for incoming messages
                            {
                                //try
                                //{

                                    byte[] msg = new byte[1024];     //the messages arrive as byte array
                                    ns.Read(msg, 0, msg.Length);   //the same networkstream reads the message sent by the client
                                    String str = System.Text.Encoding.Default.GetString(msg);

                                    foreach (String item in str.Split('\n'))
                                    {
                                        parsePhoneResponse(item);
                                        
                                    }

                                //}
                                //catch (Exception)
                                //{

                                    
                                //}
                                //ns.Write(msg, 0, msg.Length);
                            }

                            form.connStatus.Invoke((MethodInvoker)delegate
                            {
                                // Running on the UI thread
                                changeToDisconnected();
                            });

                        }
                        catch (Exception)
                        {
                            form.connStatus.Invoke((MethodInvoker)delegate
                            {
                                // Running on the UI thread
                                changeToDisconnected();
                            });
                        }
                        finally
                        {
                            isNeedToRun = false;
                            Thread.Sleep(2000);
                            isNeedToRun = true;
                        }
                    }

                }).Start();

            }
            catch (Exception)
            {
                changeToDisconnected();
                // throw;
            }
        }

        private const int latitudIndex = 5;
        private const int longtitudIndex = 8;
        private const int altitudeIndex = 10;
        private const int speedIndex = 12;


        private const int accXIndex = 5;
        private const int accYIndex = 8;
        private const int accZIndex = 11;

        private const int batteryIndex = 2;
        private const int temperatureIndex = 6;

        private const int signalIndex = 2;

        private void parsePhoneResponse(String str)
        {
            if (str.Contains("GPS"))
            {
                setGpsValues(str);
            }
            else if (str.Contains("Accelerometer"))
            {
                setACCValues(str);
            }
            else if (str.Contains("Magnetic"))
            {
                //do nothing for now 
                SetMagneticAndView(str);
            }
            else if (str.Contains("battery"))
            {
                setBatteryAndTemperature(str);
            }
            else if (str.Contains("signal"))
            {
                form.signalStrange.Invoke((MethodInvoker)delegate
                {
                    try
                    {
                        form.signalStrange.Text = "signalStrange: " + str.Split(' ')[signalIndex] + " dbm";
                    }
                    catch (Exception)
                    {
                    }
                });
            }
            else
            {
                Debug.WriteLine(str);
            }
        }

        private void SetMagneticAndView(string str)
        {
           // throw new NotImplementedException();
        }

        private static void setBatteryAndTemperature(string str)
        {
            if (str.Split(' ').Length < temperatureIndex)
            {
                return;
            }
            form.temperature.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.temperature.Text = "temperature: " + str.Split(' ')[temperatureIndex];
                }
                catch (Exception)
                {
                }
            });
            form.battery.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.battery.Text = "battery: " + str.Split(' ')[batteryIndex];
                }
                catch (Exception)
                {
                }
            });
        }

        private void setACCValues(string str)
        {
            if (str.Split(' ').Length < accZIndex)
            {
                return;
            }
            double x=0 , y=0 , z=0;
            form.Xacc.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.Xacc.Text = "X: "+str.Split(' ')[accXIndex];
                    
                    x = double.Parse(str.Split(' ')[accXIndex]);
                }
                catch (Exception)
                {
                }
            });
            form.Yacc.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.Yacc.Text = "Y: " + str.Split(' ')[accYIndex];
                    y = double.Parse(str.Split(' ')[accYIndex]);

                }
                catch (Exception)
                {
                }
            });
            form.Zacc.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.Zacc.Text = "Z: " + str.Split(' ')[accZIndex];

                    z = double.Parse(str.Split(' ')[accZIndex]);

                    AccValuesToDegree(x, y, z);

                }
                catch (Exception)
                {
                }
            });

        }

        private void setGpsValues(string str)
        {
            if (str.Split(' ').Length < longtitudIndex)
            {
                return;
            }
            form.XGPS.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.XGPS.Text = str.Split(' ')[longtitudIndex];
                    
                }
                catch (Exception)
                {
                }
            });
            form.YGPS.Invoke((MethodInvoker)delegate
            {
                try
                {
                     form.YGPS.Text = str.Split(' ')[latitudIndex];
                }
                catch (Exception)
                {
                }
            });

            form.altitudGps.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.altitudGps.Text = str.Split(' ')[altitudeIndex];
                }
                catch (Exception)
                {
                }
            });

            form.speedGPS.Invoke((MethodInvoker)delegate
            {
                try
                {
                    form.speedGPS.Text = str.Split(' ')[speedIndex];
                }
                catch (Exception)
                {
                }
            });

            form.mapCtl1.Invoke((MethodInvoker)delegate
            {
                try
                {
                    mapCtl1.MoveCenterMapObject(decimal.Parse(str.Split(' ')[longtitudIndex]), decimal.Parse(str.Split(' ')[latitudIndex]));
                    // mapCtl1.createimage(decimal.Parse(str.Split(' ')[longtitudIndex]), decimal.Parse(str.Split(' ')[latitudIndex]));
                    mapCtl1.creatpointer();
                }
                catch (Exception)
                {
                }
            });
        }

        private static void pingThread(TcpClient client)
        {
            new Thread(() =>
            {
                while (isNeedToRun)
                {
                    PingHost(((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString());
                    Thread.Sleep(1000);
                }
            }).Start();

        }

        private static void changeToConnected()
        {
            form.connStatus.Text = "device connected";
            form.connStatus.ForeColor = Color.Green;
        }

        private static void changeToDisconnected()
        {
            form.connStatus.Text = "device disconnected";
            form.connStatus.ForeColor = Color.Red;
        }

        public static void AccValuesToDegree(double x, double y, double z)
        {
           

            double pitch = Math.Atan(x / Math.Sqrt(Math.Pow(y, 2) + Math.Pow(z, 2)));
            double roll = Math.Atan(y / Math.Sqrt(Math.Pow(x, 2) + Math.Pow(z, 2)));
            //convert radians into degrees
            pitch = pitch * (180.0 / 3.14);
            roll = roll * (180.0 / 3.14);

            if (z < 0)
            {
                ivonics.SetPitchAndRollAngles(180 - pitch, roll * -1);
            }
            else
            {
                ivonics.SetPitchAndRollAngles(pitch, roll);
            }
        }


        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = null;

            try
            {
                pinger = new Ping();
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
                form.pingTxt.Invoke((MethodInvoker)delegate
                {
                    // Running on the UI thread
                    form.pingTxt.Text = "ping " + (int)(reply.RoundtripTime + (new Random().Next(4))) + " ms";
                });

            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            finally
            {
                if (pinger != null)
                {
                    pinger.Dispose();
                }
            }

            return pingable;
        }

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        var handleParam = base.CreateParams;
        //        handleParam.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED       
        //        return handleParam;
        //    }
        //}

        protected override void OnResizeBegin(EventArgs e)
        {
            SuspendLayout();
            base.OnResizeBegin(e);
        }

        protected override void OnResizeEnd(EventArgs e)
        {
            ResumeLayout();
            base.OnResizeEnd(e);
        }

        private void FrmOpticMap_Load(object sender, EventArgs e)
        {
            Text = Miscellaneous.GetAssemblyTitle();
        }

        private void FrmOpticMap_FormClosing(object sender, FormClosingEventArgs e)
        {
            mapCtl1.ControlClosing();
        }

        public void RefreshForm()
        {
            mapCtl1.RefreshControl();
        }

        public void SetCenterMapObject(decimal longitude, decimal latitude)
        {
            mapCtl1.MoveCenterMapObject(longitude, latitude);
        }

        private void buttonPanelCtl1_RefreshMapClicked(object sender, EventArgs e)
        {
            RefreshForm();
        }

        private void buttonPanelCtl1_LevelValueChanged(object sender, ButtonPanelCtl.LevelValueArgs e)
        {
            mapCtl1.Level = e.Level;
        }

        private void buttonPanelCtl1_CenterMapClicked(object sender, EventArgs e)
        {
            mapCtl1.SetCenterMap();
        }

        public int Level
        {
            set { buttonPanelCtl1.Level = value; }
        }

        private void mapCtl1_LevelValueChanged(object sender, ButtonPanelCtl.LevelValueArgs e)
        {
            buttonPanelCtl1.Level = e.Level;
        }


        private void buttonPanelCtl1_PrintMapClicked(object sender, EventArgs e)
        {
            if (printDialog1.ShowDialog() == DialogResult.OK)
                printDocument1.Print();
        }

        void Document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            e.HasMorePages = false;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBilinear;
            try
            {
                var imageMap = mapCtl1.GetMapImageForPrint();
                var printSize = e.PageBounds.Size;
                var k1 = (double)imageMap.Width / printSize.Width;
                var k2 = (double)imageMap.Height / printSize.Height;
                var k = (k1 > k2) ? k1 : k2;
                var newSize = new Size((int)(imageMap.Size.Width / k), (int)(imageMap.Size.Height / k));

                var screnCenter = new Point(printSize.Width / 2, printSize.Height / 2);
                var mapCenter = new Point(newSize.Width / 2, newSize.Height / 2);
                var shift = new Size(screnCenter.X - mapCenter.X, screnCenter.Y - mapCenter.Y);
                var p = new Point(0, 0) + shift;

                var rectangle = new Rectangle(p, newSize);
                e.Graphics.DrawImage(imageMap, rectangle);
            }
            catch (Exception ex)
            {
                //do nothing
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
        }

        private void buttonPanelCtl1_SaveAllMapClicked(object sender, EventArgs e)
        {
            FrmMapDownloader.DownloadMap();
        }

        private void buttonPanelCtl1_SaveMapAsImageClicked(object sender, EventArgs e)
        {
            FrmMapDownloader.SaveMapAsImage(mapCtl1.PiFormat, mapCtl1.Level);
        }

     


        private void runB_Click(object sender, EventArgs e)
        {
            InitBrowser(UrlTextBox.Text.ToString());
        }

        private void serverButton_Click_1(object sender, EventArgs e)
        {
            PORT = Int32.Parse(textPort.Text.ToString());
            StartServer();
        }

        private void buttonPanelCtl1_Load(object sender, EventArgs e)
        {

        }

        private void Zacc_Click(object sender, EventArgs e)
        {

        }

        private void mapCtl1_Load(object sender, EventArgs e)
        {

        }
    }
}
