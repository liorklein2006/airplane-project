﻿using ProgramMain.ExampleForms.Controls;

namespace ProgramMain.ExampleForms
{
    partial class FrmOpticMap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.speedGPS = new System.Windows.Forms.Label();
            this.pingTxt = new System.Windows.Forms.Label();
            this.connStatus = new System.Windows.Forms.Label();
            this.battery = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.temperature = new System.Windows.Forms.Label();
            this.signalStrange = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.Zacc = new System.Windows.Forms.Label();
            this.Xacc = new System.Windows.Forms.Label();
            this.altitudGps = new System.Windows.Forms.Label();
            this.XGPS = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.magnetic = new System.Windows.Forms.Label();
            this.Yacc = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.YGPS = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textPort = new System.Windows.Forms.TextBox();
            this.serverButton = new System.Windows.Forms.Button();
            this.webPannle = new System.Windows.Forms.Panel();
            this.mapCtl1 = new ProgramMain.ExampleForms.Controls.MapCtl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonPanelCtl1 = new ProgramMain.ExampleForms.Controls.ButtonPanelCtl();
            this.UrlTextBox = new System.Windows.Forms.TextBox();
            this.runB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.webPannle.SuspendLayout();
            this.SuspendLayout();
            // 
            // printDocument1
            // 
            this.printDocument1.DocumentName = "Simple Map";
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.Document_PrintPage);
            // 
            // printDialog1
            // 
            this.printDialog1.Document = this.printDocument1;
            this.printDialog1.UseEXDialog = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(2, 1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Panel2.Controls.Add(this.webBrowser1);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.buttonPanelCtl1);
            this.splitContainer1.Panel2.Controls.Add(this.UrlTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.runB);
            this.splitContainer1.Size = new System.Drawing.Size(1074, 589);
            this.splitContainer1.SplitterDistance = 359;
            this.splitContainer1.TabIndex = 3;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Location = new System.Drawing.Point(3, -4);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.webPannle);
            this.splitContainer2.Size = new System.Drawing.Size(350, 576);
            this.splitContainer2.SplitterDistance = 274;
            this.splitContainer2.TabIndex = 32;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.speedGPS);
            this.groupBox1.Controls.Add(this.pingTxt);
            this.groupBox1.Controls.Add(this.connStatus);
            this.groupBox1.Controls.Add(this.battery);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.temperature);
            this.groupBox1.Controls.Add(this.signalStrange);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.Zacc);
            this.groupBox1.Controls.Add(this.Xacc);
            this.groupBox1.Controls.Add(this.altitudGps);
            this.groupBox1.Controls.Add(this.XGPS);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.magnetic);
            this.groupBox1.Controls.Add(this.Yacc);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.YGPS);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textPort);
            this.groupBox1.Controls.Add(this.serverButton);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 268);
            this.groupBox1.TabIndex = 31;
            this.groupBox1.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(128, 9);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(152, 21);
            this.comboBox1.TabIndex = 34;
            // 
            // speedGPS
            // 
            this.speedGPS.AutoSize = true;
            this.speedGPS.Location = new System.Drawing.Point(6, 255);
            this.speedGPS.Name = "speedGPS";
            this.speedGPS.Size = new System.Drawing.Size(36, 13);
            this.speedGPS.TabIndex = 33;
            this.speedGPS.Text = "speed";
            // 
            // pingTxt
            // 
            this.pingTxt.AutoSize = true;
            this.pingTxt.Location = new System.Drawing.Point(292, 252);
            this.pingTxt.Name = "pingTxt";
            this.pingTxt.Size = new System.Drawing.Size(52, 13);
            this.pingTxt.TabIndex = 31;
            this.pingTxt.Text = "ping: 0ms";
            // 
            // connStatus
            // 
            this.connStatus.AutoSize = true;
            this.connStatus.ForeColor = System.Drawing.Color.Red;
            this.connStatus.Location = new System.Drawing.Point(6, 32);
            this.connStatus.Name = "connStatus";
            this.connStatus.Size = new System.Drawing.Size(106, 13);
            this.connStatus.TabIndex = 32;
            this.connStatus.Text = "device disconnected";
            // 
            // battery
            // 
            this.battery.AutoSize = true;
            this.battery.Location = new System.Drawing.Point(95, 252);
            this.battery.Name = "battery";
            this.battery.Size = new System.Drawing.Size(17, 13);
            this.battery.TabIndex = 30;
            this.battery.Text = "Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "port";
            // 
            // temperature
            // 
            this.temperature.AutoSize = true;
            this.temperature.Location = new System.Drawing.Point(186, 252);
            this.temperature.Name = "temperature";
            this.temperature.Size = new System.Drawing.Size(17, 13);
            this.temperature.TabIndex = 29;
            this.temperature.Text = "Z:";
            // 
            // signalStrange
            // 
            this.signalStrange.AutoSize = true;
            this.signalStrange.Location = new System.Drawing.Point(81, 229);
            this.signalStrange.Name = "signalStrange";
            this.signalStrange.Size = new System.Drawing.Size(41, 13);
            this.signalStrange.TabIndex = 23;
            this.signalStrange.Text = "label22";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(96, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Z:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(96, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "X:";
            // 
            // Zacc
            // 
            this.Zacc.AutoSize = true;
            this.Zacc.Location = new System.Drawing.Point(95, 115);
            this.Zacc.Name = "Zacc";
            this.Zacc.Size = new System.Drawing.Size(17, 13);
            this.Zacc.TabIndex = 14;
            this.Zacc.Text = "Z:";
            this.Zacc.Click += new System.EventHandler(this.Zacc_Click);
            // 
            // Xacc
            // 
            this.Xacc.AutoSize = true;
            this.Xacc.Location = new System.Drawing.Point(95, 82);
            this.Xacc.Name = "Xacc";
            this.Xacc.Size = new System.Drawing.Size(17, 13);
            this.Xacc.TabIndex = 13;
            this.Xacc.Text = "X:";
            // 
            // altitudGps
            // 
            this.altitudGps.AutoSize = true;
            this.altitudGps.Location = new System.Drawing.Point(256, 60);
            this.altitudGps.Name = "altitudGps";
            this.altitudGps.Size = new System.Drawing.Size(24, 13);
            this.altitudGps.TabIndex = 12;
            this.altitudGps.Text = "alt: ";
            // 
            // XGPS
            // 
            this.XGPS.AutoSize = true;
            this.XGPS.Location = new System.Drawing.Point(95, 60);
            this.XGPS.Name = "XGPS";
            this.XGPS.Size = new System.Drawing.Size(25, 13);
            this.XGPS.TabIndex = 11;
            this.XGPS.Text = "Lo: ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(96, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Y:";
            // 
            // magnetic
            // 
            this.magnetic.AutoSize = true;
            this.magnetic.Location = new System.Drawing.Point(7, 152);
            this.magnetic.Name = "magnetic";
            this.magnetic.Size = new System.Drawing.Size(40, 13);
            this.magnetic.TabIndex = 7;
            this.magnetic.Text = "Gravity";
            // 
            // Yacc
            // 
            this.Yacc.AutoSize = true;
            this.Yacc.Location = new System.Drawing.Point(95, 98);
            this.Yacc.Name = "Yacc";
            this.Yacc.Size = new System.Drawing.Size(17, 13);
            this.Yacc.TabIndex = 6;
            this.Yacc.Text = "Y:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Accelerometer";
            // 
            // YGPS
            // 
            this.YGPS.AutoSize = true;
            this.YGPS.Location = new System.Drawing.Point(172, 60);
            this.YGPS.Name = "YGPS";
            this.YGPS.Size = new System.Drawing.Size(31, 13);
            this.YGPS.TabIndex = 4;
            this.YGPS.Text = "Lng: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "GPS";
            // 
            // textPort
            // 
            this.textPort.Location = new System.Drawing.Point(81, 9);
            this.textPort.Name = "textPort";
            this.textPort.Size = new System.Drawing.Size(41, 20);
            this.textPort.TabIndex = 1;
            // 
            // serverButton
            // 
            this.serverButton.Location = new System.Drawing.Point(0, 6);
            this.serverButton.Name = "serverButton";
            this.serverButton.Size = new System.Drawing.Size(75, 23);
            this.serverButton.TabIndex = 0;
            this.serverButton.Text = "listen";
            this.serverButton.UseVisualStyleBackColor = true;
            this.serverButton.Click += new System.EventHandler(this.serverButton_Click_1);
            // 
            // webPannle
            // 
            this.webPannle.Controls.Add(this.mapCtl1);
            this.webPannle.Location = new System.Drawing.Point(0, 3);
            this.webPannle.Name = "webPannle";
            this.webPannle.Size = new System.Drawing.Size(347, 292);
            this.webPannle.TabIndex = 6;
            // 
            // mapCtl1
            // 
            this.mapCtl1.AllowDrop = true;
            this.mapCtl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mapCtl1.Level = 13;
            this.mapCtl1.Location = new System.Drawing.Point(0, 0);
            this.mapCtl1.Name = "mapCtl1";
            this.mapCtl1.Size = new System.Drawing.Size(347, 292);
            this.mapCtl1.TabIndex = 0;
            this.mapCtl1.LevelValueChanged += new System.EventHandler<ProgramMain.ExampleForms.Controls.ButtonPanelCtl.LevelValueArgs>(this.mapCtl1_LevelValueChanged);
            this.mapCtl1.Load += new System.EventHandler(this.mapCtl1_Load);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(705, 533);
            this.panel1.TabIndex = 7;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(6, 56);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(699, 523);
            this.webBrowser1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(554, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "url";
            // 
            // buttonPanelCtl1
            // 
            this.buttonPanelCtl1.Level = 12;
            this.buttonPanelCtl1.Location = new System.Drawing.Point(3, 2);
            this.buttonPanelCtl1.MaximumSize = new System.Drawing.Size(2000, 38);
            this.buttonPanelCtl1.MinimumSize = new System.Drawing.Size(350, 38);
            this.buttonPanelCtl1.Name = "buttonPanelCtl1";
            this.buttonPanelCtl1.Size = new System.Drawing.Size(508, 38);
            this.buttonPanelCtl1.TabIndex = 1;
            this.buttonPanelCtl1.LevelValueChanged += new System.EventHandler<ProgramMain.ExampleForms.Controls.ButtonPanelCtl.LevelValueArgs>(this.buttonPanelCtl1_LevelValueChanged);
            this.buttonPanelCtl1.CenterMapClicked += new System.EventHandler(this.buttonPanelCtl1_CenterMapClicked);
            this.buttonPanelCtl1.PrintMapClicked += new System.EventHandler(this.buttonPanelCtl1_PrintMapClicked);
            this.buttonPanelCtl1.RefreshMapClicked += new System.EventHandler(this.buttonPanelCtl1_RefreshMapClicked);
            this.buttonPanelCtl1.SaveAllMapClicked += new System.EventHandler(this.buttonPanelCtl1_SaveAllMapClicked);
            this.buttonPanelCtl1.SaveMapAsImageClicked += new System.EventHandler(this.buttonPanelCtl1_SaveMapAsImageClicked);
            this.buttonPanelCtl1.Load += new System.EventHandler(this.buttonPanelCtl1_Load);
            // 
            // UrlTextBox
            // 
            this.UrlTextBox.Location = new System.Drawing.Point(554, 16);
            this.UrlTextBox.Name = "UrlTextBox";
            this.UrlTextBox.Size = new System.Drawing.Size(148, 20);
            this.UrlTextBox.TabIndex = 4;
            this.UrlTextBox.Text = "http://192.168.43.125:8080/";
            // 
            // runB
            // 
            this.runB.Location = new System.Drawing.Point(513, 11);
            this.runB.Name = "runB";
            this.runB.Size = new System.Drawing.Size(35, 23);
            this.runB.TabIndex = 3;
            this.runB.Text = "run";
            this.runB.UseVisualStyleBackColor = true;
            this.runB.Click += new System.EventHandler(this.runB_Click);
            // 
            // FrmOpticMap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 576);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "FrmOpticMap";
            this.Text = "Map";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOpticMap_FormClosing);
            this.Load += new System.EventHandler(this.FrmOpticMap_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.webPannle.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MapCtl mapCtl1;
        private ButtonPanelCtl buttonPanelCtl1;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label connStatus;
        private System.Windows.Forms.Label pingTxt;
        private System.Windows.Forms.Label battery;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label temperature;
        private System.Windows.Forms.Label signalStrange;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label Zacc;
        private System.Windows.Forms.Label Xacc;
        private System.Windows.Forms.Label altitudGps;
        private System.Windows.Forms.Label XGPS;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label magnetic;
        private System.Windows.Forms.Label Yacc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label YGPS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textPort;
        private System.Windows.Forms.Button serverButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UrlTextBox;
        private System.Windows.Forms.Button runB;
        private System.Windows.Forms.Panel webPannle;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label speedGPS;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}