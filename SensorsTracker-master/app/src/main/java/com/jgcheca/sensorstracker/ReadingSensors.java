package com.jgcheca.sensorstracker;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;

public class ReadingSensors extends Service implements SensorEventListener, LocationListener {
    /** indicates how to behave if the service is killed */
    int mStartMode;
    /** interface for clients that bind */
    IBinder mBinder;
    /** indicates whether onRebind should be used */


    public static Socket socket;

    public static boolean isConnectedToserver ;

    private SensorManager mSensorManager;
    private Sensor mSensorAccelerometer;
    private Sensor magneticSensor;
    private Sensor mSensorGPS;

    private boolean gpsChecked ,acceChecked;

    private String SERVERIP = "";
    private int SERVERPORT;
    private Exception exception;
    private SharedPreferences preferences;
    private int raiserValueAcc=0;
    private int raiserValueGPS=0;
    private Integer seekbar_value=0;

    private LocationManager locationManager;
    private String provider;

    private static int FOREGROUND_ID=1338;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        isConnectedToserver = false;
        //Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show();
        handleCommand(intent);
        startForeground(FOREGROUND_ID,
                buildForegroundNotification());
        return Service.START_STICKY;
    }

    public void handleCommand(Intent i) {
        Bundle b = i.getExtras();
        if(b.get("GPS").equals("ENABLED"))
            gpsChecked = true;
        else
            gpsChecked = false;
        if(b.get("ACCELEROMETER").equals("ENABLED"))
            acceChecked = true;
        else
            acceChecked = false;

        seekbar_value = b.getInt("SeekBar");
        System.out.println(seekbar_value);

        if(acceChecked)
            mSensorManager.registerListener(this,mSensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        mSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
//                float gravity[] = new float[3];
//                float magnetic[] = new float[3];
//
//                magnetic[0] = event.values[0];
//                magnetic[1] = event.values[1];
//                magnetic[2] = event.values[2];
//
//                float[] R = new float[9];
//                float[] I = new float[9];
//                SensorManager.getRotationMatrix(R, I, gravity, magnetic);
//                float [] A_D = event.values.clone();
//                float [] A_W = new float[3];
//                A_W[0] = R[0] * A_D[0] + R[1] * A_D[1] + R[2] * A_D[2];
//                A_W[1] = R[3] * A_D[0] + R[4] * A_D[1] + R[5] * A_D[2];
//                A_W[2] = R[6] * A_D[0] + R[7] * A_D[1] + R[8] * A_D[2];
//
                 final float[] mMagnet = new float[3];               // magnetic field vector

                System.arraycopy(event.values, 0, mMagnet, 0, 3);         // save datas

                Log.d("gravity Field","\nX :"+mMagnet[0]+"\nY :"+mMagnet[1]+"\nZ :"+mMagnet[2]);

                new SendSensorsData(socket).execute("Value Magnetic "  + "-  x: " + mMagnet[0] + "  y: " + mMagnet[1] + " z:  " + mMagnet[2] + "  "+"\n\n");

                Intent intent = new Intent("com.yourcompany.recivemagnometic");
                intent.putExtra("X",Double.toString(mMagnet[0]));
                intent.putExtra("Y",Double.toString(mMagnet[1]));
                intent.putExtra("Z",Double.toString(mMagnet[2]));

                sendBroadcast(intent);
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        },magneticSensor, SensorManager.SENSOR_DELAY_NORMAL );
        /*locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);*/
        // Initialize the location fields
        /*if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
            locationManager.requestLocationUpdates(provider, 1000, 1, this);
        } else {
            Toast.makeText(ReadingSensors.this, "Location unavailable", Toast.LENGTH_SHORT).show();

        }*/
        if(gpsChecked)
            setGPS();

    }

    public Integer setSensorSpeed(){
        double speed = 0;

        double speeed = (Integer.MAX_VALUE/100);

        double division = speed * seekbar_value;


        speed =  (Integer.MAX_VALUE/100)*(seekbar_value)-10;




        return (int) speed;

    }

    @Override
    public void onCreate(){
        super.onCreate();
        Log.i("ReadingSensors", "Hello!!");
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        try {
            SERVERIP = preferences.getString("IpOpcion", "");
            SERVERPORT = Integer.parseInt(preferences.getString("PortOpcion", ""));
        }
        catch(Exception e){
            stopSelf();
        }

        new Thread()
        {
            public void run()
            {
                try
                {

                    socket = new Socket(SERVERIP,SERVERPORT);
                    ReadingSensors.isConnectedToserver = true;

                }
                catch (ConnectException e){
                    stopSelf();

                    isConnectedToserver = false;
                    exception = e;
                    Log.e(MainActivity.class.toString(), e.getMessage(), e);
                }
                catch (Exception ex)
                {


                    Log.e(MainActivity.class.toString(), ex.getMessage(), ex);

                }
            }

        }.start();
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (acceChecked)
            mSensorManager.unregisterListener(this, mSensorAccelerometer);
        if(gpsChecked)
            locationManager.removeUpdates(this);
        NotificationManager myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        myNotificationManager.cancel(0);
        //Toast.makeText(this, "Service Destroyed", Toast.LENGTH_SHORT).show();
        if(exception instanceof java.net.ConnectException)
            Toast.makeText(ReadingSensors.this, "Host Unreachable", Toast.LENGTH_SHORT).show();

        try {
            if (socket != null && !socket.isClosed())
                socket.close();
        } catch (Exception e) {
            Toast.makeText(this, "Could not close socket correctly", Toast.LENGTH_SHORT).show();
        }
        isConnectedToserver = false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        final float alpha = (float) 0.8;

        double[] gravity = new double[3];
        gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0];
        gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1];
        gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2];


        double[] linear_acceleration = new double[3];
        linear_acceleration[0] = event.values[0] - gravity[0];
        linear_acceleration[1] = event.values[1] - gravity[1];
        linear_acceleration[2] = event.values[2] - gravity[2];


        System.out.println("x " + linear_acceleration[0] + " y " + linear_acceleration[1] + " z " + linear_acceleration[2]);
        new SendSensorsData(socket).execute("Value Accelerometer " + raiserValueAcc + "-  x: " + linear_acceleration[0] + "  y: " + linear_acceleration[1] + " z:  " + linear_acceleration[2] + "  "+"\n\n");
        raiserValueAcc++;

        Intent intent = new Intent("com.yourcompany.reciveacc");
        intent.putExtra("X",Double.toString(linear_acceleration[0]));
        intent.putExtra("Y",Double.toString(linear_acceleration[1]));
        intent.putExtra("Z",Double.toString(linear_acceleration[2]));

        sendBroadcast(intent);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        double speed = location.getSpeed();

        double altitude = 0;

        if(location.hasAltitude())
            altitude = location.getAltitude();

        new SendSensorsData(socket).execute("Value GPS " + raiserValueGPS + "-  Latitude: " + lat + "  Longitude: " + lng + " Altitude: "+altitude+" speed: "+ speed+"  "+"\n\n");
        raiserValueGPS++;

        System.out.println("Value GPS " + raiserValueGPS + "-  Latitude: " + lat + "  Longitude: " + lng + " Altitude: "+altitude+" speed: "+ speed);


        Intent intent = new Intent("com.yourcompany.recivegps");
        intent.putExtra("Longitude",Double.toString(lng));
        intent.putExtra("Latitude",Double.toString(lat));
        intent.putExtra("altitude", Double.toString(altitude));
        intent.putExtra("speed", Double.toString(speed));

        sendBroadcast(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.OUT_OF_SERVICE:

                Toast.makeText(this, "Status Changed: Out of Service",
                        Toast.LENGTH_SHORT).show();
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:

                Toast.makeText(this, "Status Changed: Temporarily Unavailable",
                        Toast.LENGTH_SHORT).show();
                break;
            case LocationProvider.AVAILABLE:

                Toast.makeText(this, "Status Changed: Available",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(ReadingSensors.this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(ReadingSensors.this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("MissingPermission")
    public void setGPS(){
        if(locationManager==null)
            locationManager = (LocationManager) ReadingSensors.this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled=false;
        try{
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch(Exception ex){}
        boolean network_enabled=false;
        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch(Exception ex){}

        if(!gps_enabled && !network_enabled){
            Toast.makeText(ReadingSensors.this, "Activate GPS",
                    Toast.LENGTH_SHORT).show();
        }
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


        Criteria crit = new Criteria();
        crit.setPowerRequirement(Criteria.POWER_LOW);
        crit.setAccuracy(Criteria.ACCURACY_COARSE);
        String provider = locationManager.getBestProvider(crit, false);

        Criteria criteria = new Criteria();

        provider = locationManager.getBestProvider(criteria, false);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        @SuppressLint("MissingPermission") Location location = locationManager.getLastKnownLocation(provider);


        locationManager.requestLocationUpdates(provider, 500, Integer.parseInt(preferences.getString("MinDistanceOpcion","")), this);
        if (location!=null)
        {
            onLocationChanged(location);

        }


    }

    private Notification buildForegroundNotification() {
        NotificationCompat.Builder b=new NotificationCompat.Builder(this);

        b.setOngoing(true)
                .setContentTitle("running")
                .setContentText("content")
                .setSmallIcon(android.R.drawable.stat_sys_download)
                .setTicker("another text");

        return(b.build());
    }
}

class SendSensorsData extends AsyncTask<String, Integer, Void> {
    Socket socket;
    private Exception exception;

    public SendSensorsData(Socket socket){
        this.socket = socket;
    }

    protected Void doInBackground(String... urls) {
        try {

            DataOutputStream DOS = new DataOutputStream(socket.getOutputStream());
            DOS.writeUTF(urls[0]);
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
        return null;
    }
}
