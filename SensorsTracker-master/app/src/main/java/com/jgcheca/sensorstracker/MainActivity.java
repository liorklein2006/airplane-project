package com.jgcheca.sensorstracker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jgcheca.sensorstracker.Preferences.PreferencesAct;
import com.mikepenz.aboutlibraries.Libs;
import com.ragnarok.rxcamera.RxCamera;
import com.ragnarok.rxcamera.RxCameraData;
import com.ragnarok.rxcamera.config.RxCameraConfig;
import com.rey.material.app.Dialog;
import com.rey.material.widget.Button;
import com.rey.material.widget.CheckBox;

import de.cketti.library.changelog.ChangeLog;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;

public class MainActivity extends ActionBarActivity {
    private Toolbar toolbar;
    private CheckBox gpsBox;
    private CheckBox accBox;
    ChangeLog cl;
    private RxCamera camera;


    TelephonyManager mTelephonyManager;
    MyPhoneStateListener mPhoneStatelistener;
    int mSignalStrength = 0;

    class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            mSignalStrength = signalStrength.getGsmSignalStrength();
            mSignalStrength = (2 * mSignalStrength) - 113; // -> dBm

            try{
                new SendSensorsData(ReadingSensors.socket).execute("signal strength: "+mSignalStrength+"  "+"\n\n");
            }catch (Exception ex){}
        }
    }


    private TextView batteryTxt;
    private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            batteryTxt.setText("battery precent: "+String.valueOf(level) + "%");
            int temp = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,0);
            ((TextView)findViewById(R.id.temperatureTxt)).setText("battery temperature: "+ (float)(temp/10));
            if((float)temp/10 > 60)
            {
                ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP,1000);
            }


            try{
                new SendSensorsData(ReadingSensors.socket).execute("battery precent: "+String.valueOf(level) + " %"+" battery temperature: "+ (float)(temp/10)+"  "+"\n\n");
            }catch (Exception ex){}
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            //openCamera();

            mPhoneStatelistener = new MyPhoneStateListener();
            mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mTelephonyManager.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);


            batteryTxt = (TextView) this.findViewById(R.id.batteryTxt);
            this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));



            toolbar = (Toolbar) findViewById(R.id.toolbar);
            if (toolbar != null) {
                toolbar.setTitle(getResources().getString(R.string.app_name));
                setSupportActionBar(toolbar);
            }
            //toolbar.setTitleTextColor(getResources().getColor(R.color.md_white_1000));

            Button startService_btn = (Button) findViewById(R.id.start_btn);
            Button stopService_btn = (Button) findViewById(R.id.stop_btn);
            gpsBox = (CheckBox) findViewById(R.id.checkbox_gps);
            accBox = (CheckBox) findViewById(R.id.checkbox_accelerometer);

            startService_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                        startService();

                }
            });

            stopService_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                       stopService();

                }
            });
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//
//                        }
//                    });
//                    while (true)
//                    {
//                        try {
//                            if(!ReadingSensors.isConnectedToserver) {
//
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        stopService();
//                                        startService();
//                                    }
//                                });
//
//                            }
//                            Thread.sleep(10000);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            }).start();





            cl = new ChangeLog(this);
            if (cl.isFirstRun()) {
                cl.getLogDialog().show();
            }



            IntentFilter filter = new IntentFilter("com.yourcompany.recivegps");
            receiver1 = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String Longitude =  intent.getExtras().getString("Longitude");
                    String Latitude =  intent.getExtras().getString("Latitude");
                    String speed = intent.getExtras().getString("speed");
                    String altitude = intent.getExtras().getString("altitude");

                    ((TextView)findViewById(R.id.GpsText)).setText("long: "+Longitude+" Latitiud: "+ Latitude);
                    ((TextView)findViewById(R.id.speedTxt)).setText("speed: "+speed);
                    ((TextView)findViewById(R.id.altitudeText)).setText("altitude: "+altitude);
                }
            };
            registerReceiver(receiver1, filter);

            IntentFilter filteracc2 = new IntentFilter("com.yourcompany.reciveacc");
             receiveracc = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String X =  intent.getExtras().getString("X");
                    String Y =  intent.getExtras().getString("Y");
                    String Z =  intent.getExtras().getString("Z");

                    ((TextView)findViewById(R.id.accText)).setText("acc: x- "+X+" y- "+ Y+" z- "+Z);
                }
            };
            registerReceiver(receiveracc, filteracc2);

            IntentFilter filtermag = new IntentFilter("com.yourcompany.recivemagnometic");
             receivermag3 = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String X =  intent.getExtras().getString("X");
                    String Y =  intent.getExtras().getString("Y");
                    String Z =  intent.getExtras().getString("Z");

                    ((TextView)findViewById(R.id.magneticTxt)).setText("mag: x- "+X+" y- "+ Y+" z- "+Z);
                }
            };
            registerReceiver(receivermag3, filtermag);


        }catch (Exception ex){
            Log.e("errrrororrorooror", ex.getMessage());}
    }
    BroadcastReceiver receiver1,receiveracc,receivermag3 ;
    public void stopService()
    {
        if (!stopService(new Intent(MainActivity.this, ReadingSensors.class)))
            Toast.makeText(MainActivity.this, getResources().getString(R.string.stopServiceToast), Toast.LENGTH_SHORT).show();
    }


    public void startService() {

        final Dialog mDialog = new Dialog(MainActivity.this);
        int slider_value = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getInt("SEEKBAR_VALUE", 50);
        String IP = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("IpOpcion", "");
        String Port = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).getString("PortOpcion", "");
        //Start Service
        Intent i = new Intent(MainActivity.this, ReadingSensors.class);
        i.putExtra("SeekBar", slider_value);
        // add data to the intent if you want
        Toast.makeText(this, "server ip:"+ IP+":"+Port, Toast.LENGTH_SHORT).show();



        if (!accBox.isChecked() && !gpsBox.isChecked()) {
            mDialog
                    .title(getResources().getString(R.string.dialog_sensors_unavailable))
                    .positiveAction("OK")
                    .cancelable(true)
                    .elevation(20)
                    .show();

            mDialog.positiveActionClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
        } else {

            if (Port.equals("") && IP.equals("")) {
                mDialog
                        .title(getResources().getString(R.string.dialog_setIPPort))
                        .positiveAction("OK")
                        .cancelable(true)
                        .elevation(20)
                        .show();
                mDialog.positiveActionClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });

            } else if (Port.equals("")) {
                mDialog
                        .title(getResources().getString(R.string.dialog_set_port))
                        .positiveAction("OK")
                        .cancelable(true)
                        .elevation(20)
                        .show();
                mDialog.positiveActionClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });

            } else if (IP.equals("")) {
                mDialog
                        .title(getResources().getString(R.string.dialog_set_IP))
                        .positiveAction("OK")
                        .elevation(20)
                        .cancelable(true)
                        .show();
                mDialog.positiveActionClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
            } else {


                if (gpsBox.isChecked()) {

                    LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
                    boolean enabled = service
                            .isProviderEnabled(LocationManager.GPS_PROVIDER);
                    if (!enabled) {


                        mDialog
                                .title(getResources().getString(R.string.dialog_GPS_title))
                                .positiveAction("OK")
                                .negativeAction(getResources().getString(R.string.cancel))
                                .elevation(20)
                                .cancelable(true)
                                .show();

                        mDialog.positiveActionClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                mDialog.dismiss();
                            }
                        });
                        mDialog.negativeActionClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();
                            }
                        });
                    } else {
                        i.putExtra("GPS", "ENABLED");
                        if (accBox.isChecked())
                            i.putExtra("ACCELEROMETER", "ENABLED");
                        else
                            i.putExtra("ACCELEROMETER", "DISABLED");

                        startService(i);
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        PendingIntent pIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);

                        Notification n = new NotificationCompat.Builder(MainActivity.this)
                                .setContentTitle(getResources().getString(R.string.Notification_title))
                                .setContentText(getResources().getString(R.string.Notification_subject))
                                .setSmallIcon(R.mipmap.ic_launcher)

                                .setAutoCancel(true)
                                .build();

                        n.flags |= Notification.FLAG_NO_CLEAR;
                        NotificationManager notificationManager =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                        notificationManager.notify(0, n);

                    }


                } else if (accBox.isChecked()) {

                    i.putExtra("GPS", "DISABLED");
                    i.putExtra("ACCELEROMETER", "ENABLED");


                    startService(i);
                    Intent intent = new Intent(MainActivity.this, MainActivity.class);
                    PendingIntent pIntent = PendingIntent.getActivity(MainActivity.this, 0, intent, 0);

                    Notification n = new NotificationCompat.Builder(MainActivity.this)
                            .setContentTitle(getResources().getString(R.string.Notification_title))
                            .setContentText(getResources().getString(R.string.Notification_subject))
                            .setSmallIcon(R.mipmap.ic_launcher)

                            .setAutoCancel(true)
                            .build();

                    n.flags |= Notification.FLAG_NO_CLEAR;
                    NotificationManager notificationManager =
                            (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                    notificationManager.notify(0, n);
                }


            }
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver1);
        unregisterReceiver(receiveracc);
        unregisterReceiver(receivermag3);
unregisterReceiver(this.mBatInfoReceiver);
        stopService(new Intent(MainActivity.this, ReadingSensors.class));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
           /* Intent i = new Intent(MainActivity.this, MyPreferenceActivity.class);
            startActivity(i);*/
            Intent intent = new Intent(this, PreferencesAct.class);
            startActivity(intent);
           return true;

        }
        if(id == R.id.action_library){
            new Libs.Builder()
                    //Pass the fields of your application to the lib so it can find all external lib information
                    .withFields(R.string.class.getFields())
                    .withVersionShown(true)
                    .withLicenseShown(true)
                    .withAutoDetect(true)
                    .withActivityTitle(getResources().getString(R.string.action_library))
                    .withAboutAppName(getResources().getString(R.string.app_name))
                    .withAboutIconShown(true)
                    .withAboutVersionShown(true)
                    .withAboutDescription(getResources().getString(R.string.license_description)+"<br /><b>UCLM - ESI</b>")

                    .withAnimations(true)
                    .withAutoDetect(true)
                    .withActivityTheme(R.style.Base_Theme_AppCompat_Light)
                    .start(this);
            return true;
        }

        if(id == R.id.action_changelog){
            new ChangeLog(this).getFullLogDialog().show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void openCamera() {
        RxCameraConfig config = new RxCameraConfig.Builder()
                .useBackCamera()
                .setAutoFocus(true)
                .setPreferPreviewFrameRate(15, 30)
                .setPreferPreviewSize(new Point(640, 480), false)
                .setHandleSurfaceEvent(true)
                .build();
        Log.d("debag", "config: " + config);
        RxCamera.open(this, config).flatMap(new Func1<RxCamera, Observable<RxCamera>>() {
            @Override
            public Observable<RxCamera> call(RxCamera rxCamera) {
                showLog("isopen: " + rxCamera.isOpenCamera() + ", thread: " + Thread.currentThread());
                camera = rxCamera;
                return rxCamera.bindTexture((TextureView)(findViewById(R.id.texture)));
            }
        }).flatMap(new Func1<RxCamera, Observable<RxCamera>>() {
            @Override
            public Observable<RxCamera> call(RxCamera rxCamera) {
                showLog("isbindsurface: " + rxCamera.isBindSurface() + ", thread: " + Thread.currentThread());
                return rxCamera.startPreview();
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<RxCamera>() {
            @Override
            public void onCompleted() {
                camera.action().flashAction(true);

            }

            @Override
            public void onError(Throwable e) {
                showLog("open camera error: " + e.getMessage());
            }

            @Override
            public void onNext(final RxCamera rxCamera) {
                camera = rxCamera;
                showLog("open camera success: " + camera);
                Toast.makeText(MainActivity.this, "Now you can tap to focus", Toast.LENGTH_LONG).show();

                if (!checkCamera()) {
                    showLog("problem with camera");
                }
                camera.action().flashAction(false).subscribe(new Subscriber<RxCamera>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        showLog("open flash error: " + e.getMessage());
                    }

                    @Override
                    public void onNext(RxCamera rxCamera) {
                        showLog("open flash");
                    }
                });

                camera.request().successiveDataRequest().subscribe(new Action1<RxCameraData>() {
                    @Override
                    public void call(RxCameraData rxCameraData) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(rxCameraData.cameraData, 0, rxCameraData.cameraData.length);
                        ((ImageView)findViewById(R.id.imageview)).setImageBitmap(bitmap);
                        showLog("successiveData, cameraData.length: " + rxCameraData.cameraData.length);
                    }
                });

            }
        });
    }
    private void showLog(String s) {
        Log.e("debug camera", s);

    }
    private boolean checkCamera() {
        if (camera == null || !camera.isOpenCamera()) {
            return false;
        }
        return true;
    }

}
